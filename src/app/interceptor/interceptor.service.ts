import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class InterceptorService {

  constructor(private http:Http) {
  }

  createAuthorizationHeader(headers:Headers) {
    let token = localStorage.getItem('token');
    headers.append('Authorization', 'Bearer ' + token);
  }

  get(url) {
    let getHeaders = new Headers();
    this.createAuthorizationHeader(getHeaders);
    return this.http.get(url, {headers: getHeaders});
  }

  post(url, data) {
    let postHeaders = new Headers();
    this.createAuthorizationHeader(postHeaders);
    return this.http.post(url, data, {headers: postHeaders});
  }

  put(url, data) {
    let puHeaders = new Headers();
    this.createAuthorizationHeader(puHeaders);
    return this.http.put(url, data, {headers: puHeaders});
  }

  delete(url) {
    let deleteHeaders = new Headers();
    this.createAuthorizationHeader(deleteHeaders);
    return this.http.delete(url, {headers: deleteHeaders});
  }
}
