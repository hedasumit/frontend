import {Injectable, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Response} from '@angular/http';
import {AppConfig} from './../app.config';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class LoginService {

  // Injecting the http client into the service
  constructor(private http:Http) {

  }

  /**
   * Login method of the service
   * @param {{}} data
   * @returns {Observable<any[]>}
   */
  logIn(data:{}):Observable<any []> {
    return this.http.post(AppConfig.API_ENDPOINT + 'auth/login', data).map(response => {
      // insert data into local storage the token and the current user
      // this.getData = this.getData.json();
      return response.json();
      // Catch method to throw the error details
    }).catch((err:Response) => {
        const details = err.json();
        return Observable.throw(details);
      });
  }


}
