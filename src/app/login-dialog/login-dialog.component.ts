import {Component, OnInit, Inject, Optional } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {LoginService} from './login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit{

  user: any = {};
  constructor(public dialogRef: MatDialogRef<LoginDialogComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
                  private router: Router,
                private loginService: LoginService) { }


login() {
  this.loginService.logIn({email: this.user.email, password: this.user.password}).subscribe((response:any) => {
    localStorage.setItem('token', response.token);
    // isAuthenticate emit for get logged in user's data
    if (this.data) {
      this.dialogRef.close(false);
      this.router.navigate(['/']);
    } else {
      this.dialogRef.close(true);
    }
  }, (error) => {
    // If there is some error then close the modal and stay in same state
    this.dialogRef.close(false);
  });
}
ngOnInit(){
}

}
