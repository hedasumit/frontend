import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routes} from './app.routes'
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DialogsService} from './dialog-service/dialogs.service';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { LoginService } from './login-dialog/login.service';
import { InterceptorService } from './interceptor/interceptor.service';
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';
import { RegisterService } from './register-dialog/register.service';
import {
  MatButtonModule,
  MatDialogModule
} from '@angular/material';
@NgModule({
  imports: [
    HttpModule,
    RouterModule,
    RouterModule.forRoot(routes),
    BrowserModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    LoginDialogComponent,
    RegisterDialogComponent
  ],
  providers: [InterceptorService, DialogsService, LoginService,RegisterService],
  bootstrap: [AppComponent],
  exports: [],
  entryComponents: [LoginDialogComponent,RegisterDialogComponent]
})
export class AppModule {
}
