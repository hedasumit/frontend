import { Component } from '@angular/core';
import { DialogsService } from './dialog-service/dialogs.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(public dialogsService:DialogsService) {
  }

  loginNewUser() {
    this.dialogsService.login().subscribe(res => {
    });
  }

  registerNewUser() {
    this.dialogsService.register().subscribe(res => {
    });
  }
}
