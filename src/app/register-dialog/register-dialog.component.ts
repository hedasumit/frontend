import {Component, OnInit, Inject, Optional } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {RegisterService} from './register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.css']
})
export class RegisterDialogComponent implements OnInit {

  user: any = {};
  constructor(public dialogRef: MatDialogRef<RegisterDialogComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  private router: Router,
  private registerService: RegisterService) { }

  ngOnInit() {
  }

register() {
  if(this.user.password === this.user.confirmPassword) {
    this.registerService.register(this.user).subscribe((response:any) => {
      if (this.data) {
        this.dialogRef.close(false);
        this.router.navigate(['/']);
      } else {
        this.dialogRef.close(true);
      }
    }, (error) => {
      // If there is some error then close the modal and stay in same state
      this.dialogRef.close(false);
    });
  }

}

}
