import {Observable} from 'rxjs/Rx';
import {MatDialogRef, MatDialog, MatDialogConfig} from '@angular/material';
import {Injectable} from '@angular/core';
import {LoginDialogComponent} from '../login-dialog/login-dialog.component';
import {RegisterDialogComponent} from '../register-dialog/register-dialog.component';
@Injectable()
export class DialogsService {
  constructor(private dialog:MatDialog) {
  }

  public login():Observable<boolean> {
    let dialogRef:MatDialogRef<LoginDialogComponent>;
    dialogRef = this.dialog.open(LoginDialogComponent);
    return dialogRef.afterClosed();
  }

  public register():Observable<boolean> {
    let dialogRef:MatDialogRef<RegisterDialogComponent>;
    dialogRef = this.dialog.open(RegisterDialogComponent);
    return dialogRef.afterClosed();
  }
}
